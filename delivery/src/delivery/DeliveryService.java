package delivery;

import invoicing.InvoiceService.InvoiceService;
import invoicing.InvoiceService.impl.InvoiceServiceImpl;
import invoicing.model.Invoice;

import java.util.List;


public class DeliveryService {

    private InvoiceService invoiceService = InvoiceService.build();



    public void deliver(){
        List<Invoice> invoices = invoiceService.generatedInvoice();
        invoices.forEach(System.out::println);

    }


}
