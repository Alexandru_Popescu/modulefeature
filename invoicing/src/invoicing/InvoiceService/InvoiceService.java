package invoicing.InvoiceService;

import invoicing.InvoiceService.impl.InvoiceServiceImpl;
import invoicing.model.Invoice;

import java.util.List;

public interface InvoiceService {

    List<Invoice> generatedInvoice();


    static InvoiceService build() {
       return new InvoiceServiceImpl();

    }
}
